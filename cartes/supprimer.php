<?php

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST');
header('Access-Control-Max-Age: 1000');
header('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');

if (!empty($_POST['url']) && !empty($_POST['token'])) {
	require 'db.php';
	$url = $_POST['url'];
	$token = $_POST['token'];
	$stmt = $db->prepare('DELETE FROM digitools WHERE url = :url AND token = :token');
	if ($stmt->execute(array('url' => $url, 'token' =>  $token))) {
		echo 'termine';
	} else {
		echo 'erreur';
	}
	$db = null;
	exit();
} else {
	header('Location: ' . explode('?', $_SERVER['REQUEST_URI'])[0]);
}

?>
