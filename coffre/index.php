<?php

$url = '';
$parametres = '';
$token = '';
$admin = 0;

if (!empty($_GET['p'])) {
	require 'db.php';
	if (!empty($_GET['t'])) {
		$token = $_GET['t'];
	}
	$url = $_GET['p'];
	$stmt = $db->prepare('SELECT token, parametres FROM digitools WHERE url = :url');
	if ($stmt->execute(array('url' => $url))) {
		$resultat = $stmt->fetchAll();
		if (!$resultat) {
			header('Location: ' . explode('?', $_SERVER['REQUEST_URI'])[0]);
			$db = null;
			return false;
		}
		if ($resultat[0]['parametres'] && $resultat[0]['parametres'] !== '') {
			$parametres = rawurlencode($resultat[0]['parametres']);
		} else {
			header('Location: ' . explode('?', $_SERVER['REQUEST_URI'])[0]);
		}
		if ($resultat[0]['token'] && $resultat[0]['token'] !== '' && $token !== '' && $resultat[0]['token'] === $token) {
			$admin = 1;
		} else if ($resultat[0]['token'] && $resultat[0]['token'] !== '' && $token !== '' && $resultat[0]['token'] !== $token) {
			header('Location: ' . explode('?', $_SERVER['REQUEST_URI'])[0]);
		}
	}
	$db = null;
}

?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, height=device-height, viewport-fit=cover, initial-scale=1.0, maximum-scale=5.0">
		<meta name="description" content="Un outil de la suite Digitools pour créer un coffre au trésor.">
        <title>Coffre-fort - Digitools by La Digitale</title>
		<link rel="stylesheet" href="../destyle.css" type="text/css">
		<link rel="stylesheet" href="../commun.css?v=1672584771480" type="text/css">
        <link rel="stylesheet" href="style.css" type="text/css">
		<link rel="icon" type="image/png" href="../favicon.png">
    </head>

	<body>
		<header>
			<a href="../" title="Retour"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" focusable="false" width="32px" height="32px" style="-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); transform: rotate(360deg);" preserveAspectRatio="xMidYMid meet" viewBox="0 0 16 16"><g fill="#ffffff"><path fill-rule="evenodd" d="M14 1H2a1 1 0 0 0-1 1v12a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1zM2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2z"/><path fill-rule="evenodd" d="M8.354 11.354a.5.5 0 0 0 0-.708L5.707 8l2.647-2.646a.5.5 0 1 0-.708-.708l-3 3a.5.5 0 0 0 0 .708l3 3a.5.5 0 0 0 .708 0z"/><path fill-rule="evenodd" d="M11.5 8a.5.5 0 0 0-.5-.5H6a.5.5 0 0 0 0 1h5a.5.5 0 0 0 .5-.5z"/></g><rect x="0" y="0" width="16" height="16" fill="rgba(0, 0, 0, 0)" /></svg></a>
			<span>Coffre-fort</span>
		</header>
		<main>
			<div class="conteneur conteneur-consigne">
				<p>Indiquez la combinaison (texte ou chiffres) pour ouvrir le coffre et le trésor obtenu (texte ou lien vers une image) dans les champs ci-dessous.</p>
				<p class="petit">Digitools n'héberge aucun contenu personnel, mais vous pouvez utiliser des services comme <a href="https://postimages.org" target="_blank" rel="noreferrer"><i>Postimage</i></a> pour téléverser et utiliser votre propre image pour le trésor.</p>
			</div>
			<div class="conteneur conteneur-edition">
				<div id="champs">
					<div id="champ1">
						<label for="champ-combinaison">Combinaison</label>
						<input id="champ-combinaison" type="text">
					</div>
					<div id="champ2">
						<label for="champ-tresor">Trésor</label>
						<input id="champ-tresor" type="text">
					</div>
					<div id="champ3">
						<label for="champ-lien-tresor">Lien hypertexte pour le trésor (facultatif)</label>
						<input id="champ-lien-tresor" type="text">
					</div>
				</div>
				<div class="actions">
					<span id="recommencer" class="bouton" role="button" tabindex="0" onClick="recommencer()">Recommencer</span>
					<span id="creer" class="bouton" role="button" tabindex="0" onClick="creer()">Valider</span>
				</div>
			</div>
			<div class="conteneur-affichage">
				<div class="conteneur-affichage-coffre">
					<div class="conteneur-coffre">
						<div id="actions">
							<input type="text" id="combinaison" placeholder="Combinaison">
							<span id="valider" class="bouton" role="button" tabindex="0" onClick="valider()">Vérifier</button>
						</div>

						<div id="coffre">
							<div id="barre"></div>
						</div>
						
						<div id="recompense" class="texte defaut">
							Trésor
						</div>
					</div>
				</div>
			</div>
			<div class="conteneur conteneur-quitter">
				<div class="actions">
					<span id="quitter" class="bouton" role="button" tabindex="0" onClick="quitter()">Quitter</span>
				</div>
			</div>
		</main>
		<footer>
			<p>Cette page 100% cousue main ne collecte aucune donnée et n'utilise aucun cookie.</p>
			<p><?php echo date("Y"); ?> - <a href="https://ladigitale.dev" target="_blank" rel="noreferrer">La Digitale</a></p>
			<a href="../">Retour à la liste des outils</a>
		</footer>
		
		<script type="text/javascript" src="../clipboard.js"></script>
		<script>
			let combinaison = ''
			let tresor = ''
			let lienTresor = ''
			let url = ''
			let token = ''
			const hote = window.location.href.split('?')[0]
			const coffre = document.querySelector('#coffre')
			const barre = document.querySelector('#barre')
			const recompense = document.querySelector('#recompense')
			coffre.style.left = '-40px'
			
			document.querySelector('#champ2 input').addEventListener('keyup', definirRecompenseClavier, false)
			document.querySelector('#champ3 input').addEventListener('keyup', definirRecompenseClavier, false)
			document.querySelector('#combinaison').addEventListener('keypress', function (event) {
				if (event.key === 'Enter') {
					valider()
				}
			})
			
			function definirRecompenseClavier () {
				tresor = document.querySelector('#champ2 input').value
				lienTresor = document.querySelector('#champ3 input').value
				definirRecompense(tresor, lienTresor)
			}
			
			function definirRecompense (valeur, valeurLien) {
				recompense.classList = ''
				recompense.classList.remove('defaut')
				if (verifierURL(valeur) === false) {
					recompense.classList.add('texte')
					if (valeurLien !== '') {
						recompense.innerHTML = '<a href="' + valeurLien + '" target="_blank">' + valeur + '</a>'
					} else {
						recompense.innerHTML = valeur
					}
				} else {
					recompense.classList.add('image')
					if (valeurLien !== '') {
						recompense.innerHTML = '<a href="' + valeurLien + '" target="_blank"><img src="' + valeur + '"></a>'
					} else {
						recompense.innerHTML = '<img src="' + valeur + '">'
					}
				}
			}
			
			function creer (action) {
				if (action === 'lire') {
					if (combinaison !== '') {
						document.querySelector('.conteneur-consigne').style.display = 'none'
						document.querySelector('.conteneur-edition').style.display = 'none'
						document.querySelector('.conteneur-quitter').style.display = 'block'
						coffre.style.transition = 'left 0.5s ease-in-out'
						coffre.style.transitionDelay = '0s'
						coffre.style.left = '435px'
					}
				} else {
					combinaison = document.querySelector('#champ1 input').value.toLowerCase()
					if (combinaison !== '') {
						const xhr = new XMLHttpRequest()
						xhr.onload = function () {
							if (xhr.readyState === xhr.DONE && xhr.status === 200) {
								if (xhr.responseText !== 'erreur') {
									const donnees = JSON.parse(xhr.responseText)
									url = donnees.url
									token = donnees.token
									window.location = hote + '?p=' + url + '&t=' + token
								} else {
									alert('Erreur de communication avec le serveur.')
								}
							} else {
								alert('Erreur de communication avec le serveur.')
							}
						}
						xhr.open('POST', hote + 'enregistrer.php', true)
						xhr.setRequestHeader('Content-type', 'application/json')
						xhr.send(JSON.stringify({ url: url, token: token, parametres: JSON.stringify({ combinaison: combinaison, tresor: encodeURIComponent(tresor), lienTresor: encodeURIComponent(lienTresor) }) }))
					}
				}
			}
			
			function valider () {
				barre.style.transition = 'transform 1.5s ease-in-out'
				barre.style.transform = 'rotate(360deg)'
				const ouverture = document.createElement('audio')
				ouverture.src = './ouverture.mp3'
				ouverture.play()
				if (document.querySelector('#combinaison').value.toLowerCase() !== '' && document.querySelector('#combinaison').value.toLowerCase() === combinaison) {
					coffre.style.transition = 'left 2s ease-in-out'
					coffre.style.transitionDelay = '1.5s'
					coffre.style.left = '-40px'
					setTimeout(function () {
						const victoire = document.createElement('audio')
						victoire.src = './victoire.mp3'
						victoire.play()
					}, 2000)
				} else {
					setTimeout(function () {
						const erreur = document.createElement('audio')
						erreur.src = './erreur.mp3'
						erreur.play()
						barre.style.transition = 'transform 1.5s ease-in-out'
						barre.style.transitionDelay = '2s'
						barre.style.transform = 'rotate(0)'
						document.querySelector('#combinaison').value = ''
					}, 1500)
				}
			}
			
			function quitter () {
				document.querySelector('#creer').textContent = 'Modifier'
				document.querySelector('.conteneur-consigne').style.display = 'block'
				document.querySelector('.conteneur-edition').style.display = 'block'
				document.querySelector('.conteneur-quitter').style.display = 'none'
				coffre.style.left = '-40px'
			}
			
			function recommencer () {
				window.location = hote
			}
			
			function genererLiens (url, token) {
				const lienAdmin = hote + '?p=' + url + '&t=' + token
				const lien = hote + '?p=' + url
				const element = document.createElement('div')
				element.id = 'liens'
				element.innerHTML = '<label>Lien d\'administration</label><input type="text" id="lien-admin" value="' + lienAdmin + '" disabled><span id="copier-admin" role="button" tabindex="0"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="black" width="18px" height="18px"><path d="M0 0h24v24H0z" fill="none"></path><path d="M16 1H4c-1.1 0-2 .9-2 2v14h2V3h12V1zm3 4H8c-1.1 0-2 .9-2 2v14c0 1.1.9 2 2 2h11c1.1 0 2-.9 2-2V7c0-1.1-.9-2-2-2zm0 16H8V7h11v14z"></path></svg></span><label>Lien de partage</label><input type="text" id="lien" value="' + lien + '" disabled><span id="copier" role="button" tabindex="0"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="black" width="18px" height="18px"><path d="M0 0h24v24H0z" fill="none"></path><path d="M16 1H4c-1.1 0-2 .9-2 2v14h2V3h12V1zm3 4H8c-1.1 0-2 .9-2 2v14c0 1.1.9 2 2 2h11c1.1 0 2-.9 2-2V7c0-1.1-.9-2-2-2zm0 16H8V7h11v14z"></path></svg></span><div id="supprimer"><span class="bouton" role="button" tabindex="0" onClick="supprimer()">Supprimer</span></div>'
				document.querySelector('footer').insertAdjacentElement('beforebegin', element)
				const clipboardAdmin = new ClipboardJS('#copier-admin', {
					text: function () {
						return lienAdmin
					}
				})
				clipboardAdmin.on('success', function () {
					const element = document.createElement('div')
					const id = 'notification_' + Date.now().toString(36) + Math.random().toString(36).substring(2)
					element.id = id
					element.textContent = 'Lien d\'administration copié dans le presse-papier.'
					element.classList.add('notification')
					document.querySelector('main').appendChild(element)
					setTimeout(function () {
						element.parentNode.removeChild(element)
					}, 2500)
				})
				const clipboardLien = new ClipboardJS('#copier', {
					text: function () {
						return lien
					}
				})
				clipboardLien.on('success', function () {
					const element = document.createElement('div')
					const id = 'notification_' + Date.now().toString(36) + Math.random().toString(36).substring(2)
					element.id = id
					element.textContent = 'Lien de partage copié dans le presse-papier.'
					element.classList.add('notification')
					document.querySelector('main').appendChild(element)
					setTimeout(function () {
						element.parentNode.removeChild(element)
					}, 2500)
				})
			}

			function supprimer () {
				if (confirm('Souhaitez-vous vraiment supprimer ce contenu ?')) {
					const xhr = new XMLHttpRequest()
					xhr.onload = function () {
						if (xhr.readyState === xhr.DONE && xhr.status === 200) {
							if (xhr.responseText !== 'erreur') {
								window.location = hote
							} else {
								alert('Erreur de communication avec le serveur.')
							}
						} else {
							alert('Erreur de communication avec le serveur.')
						}
					}
					xhr.open('POST', hote + 'supprimer.php', true)
					xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded')
					xhr.send('url=' + url + '&token=' + token)
				} 
			}
			
			function verifierURL (str) {
				const pattern = new RegExp('^(https?:\\/\\/)?'+
					'((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|'+
					'((\\d{1,3}\\.){3}\\d{1,3}))'+
					'(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*'+
					'(\\?[;&a-z\\d%_.~+=-]*)?'+
					'(\\#[-a-z\\d_]*)?$','i')
				return !!pattern.test(str)
			}
			
			function redimensionner () {
				const element = document.querySelector('.conteneur-affichage-coffre')
				const calc = (window.innerWidth - 1440) / 2
				element.style.transform = 'translateX(' + calc + 'px)'
			}
			
			redimensionner()
			
			window.addEventListener('resize', redimensionner, false)
			
			window.addEventListener('load', function () {
				if ('<?php echo $url; ?>' !== '' && '<?php echo $parametres; ?>' !== '') {
					url = '<?php echo $url; ?>'
					const donnees = JSON.parse(decodeURIComponent('<?php echo $parametres; ?>'))
					combinaison = donnees.combinaison
					tresor = decodeURIComponent(donnees.tresor)
					document.querySelector('#champ1 input').value = combinaison
					document.querySelector('#champ2 input').value = tresor
					if (donnees.hasOwnProperty('lienTresor')) {
						lienTresor = decodeURIComponent(donnees.lienTresor)
						document.querySelector('#champ3 input').value = lienTresor
					}
					definirRecompense(tresor, lienTresor)
					creer('lire')
				} else {
					document.querySelector('#recommencer').style.display = 'none'
				}
				if (parseInt('<?php echo $admin; ?>') === 1) {
					token = '<?php echo $token; ?>'
					if (window === window.parent) {
						genererLiens(url, token)
					}
				} else {
					document.querySelector('#quitter').parentNode.removeChild(document.querySelector('#quitter'))
				}
				document.body.style.display = 'block'
			})

			if (window !== window.parent) {
				document.querySelector('header').style.display = 'none'
				document.querySelector('footer').style.display = 'none'
			}
		</script>
	</body>
</html>
